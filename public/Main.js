const path = require("path");
const electron = require("electron");
const { app, BrowserWindow, ipcMain } = electron;
const { exec } = require("child_process");

function execute(command, callback) {
  exec(command, (error, stdout, stderr) => {
    callback(stdout);
  });
}

var getResourcesPath = function() {
  var paths = Array.from(arguments);

  if (/[\\/]Electron\.app[\\/]/.test(process.execPath)) {
    // Development mode resources are located in project root.
    paths.unshift(process.cwd());
  } else {
    // In builds the resources directory is located in 'Contents/Resources'
    paths.unshift(process.resourcesPath);
  }

  return path.join.apply(null, paths);
};

var extraResources = getResourcesPath("service");

let win;

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 400,
    height: 600,
    backgroundColor: "#2e2c29",
    resizable: false,
    fullscreen: false,
    fullscreenable: false,
    webPreferences: {
      nodeIntegration: true,
      preload: __dirname + "/preload.js"
    }
    // frame: false
    // titleBarStyle: "hidden"
  });

  // and load the index.html of the app.
  // win.loadFile('index.html')
  console.log("ENV", process.env.NODE_ENV);
  console.log("__dirname", __dirname);

  // Open the DevTools.
  // win.webContents.openDevTools();

  if (process.env.NODE_ENV === "development")
    win.loadURL("http://localhost:3000/");
  else win.loadURL(`file://${path.join(__dirname, "../build/index.html")}`);
}
app.on("ready", createWindow);

//IPC communication.
ipcMain.on("invokeAction", function(event, data) {
  console.log("data", data);
  const executableFile = `${extraResources}/calc`;
  // const executableFile = path.join(
  //   process.resourcePath,
  //   "extraResources",
  //   "calc"
  // );
  // const executableFile = path.join(
  //   path.dirname(__dirname),
  //   "extraResources",
  //   "calc"
  // );
  // const executableFile =
  //   process.env.NODE_ENV === "development"
  //     ? "../service/calc"
  //     : "./service/calc";
  console.log(executableFile);

  // call the function
  execute(`${executableFile} ${data}`, output => {
    console.log("calc output", output);
    event.sender.send("actionReply", output);
  });
});
