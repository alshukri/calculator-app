import React, { useState, useEffect } from "react";
import "./App.css";
import { ThemeProvider } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";

import { theme } from "./styles/theme";
import { windowHeight, windowWidth } from "./constants/window";

import ResultsScreen from "./components/ResultsScreen";
import Keypad from "./components/Keypad";
import isElectron from "is-electron";

function App() {
  const [calcResult, setCalcResult] = useState();
  const [aggregateKeys, setAggregateKeys] = useState([]);
  const [keyPressed, setKeyPressed] = useState("");
  const [lastKeyPressed, setLastKeyPressed] = useState();

  useEffect(() => {
    if (isElectron()) {
      console.log(window.ipcRenderer);
      window.ipcRenderer.on("actionReply", function(event, response) {
        setCalcResult(response);
        setAggregateKeys([]);
        setKeyPressed("");
      });
    }

    return () => {
      if (isElectron()) {
        window.ipcRenderer.removeListener("actionReply", function(
          event,
          response
        ) {
          console.log("Unmount", response);
        });
      }
    };
  }, []);

  const onKeyPress = item => () => {
    if (!item) return;

    const keys = [...aggregateKeys];
    let lastItem = keys.length - 1;

    if (item.type === "operator") {
      if (lastKeyPressed && lastKeyPressed.type === "operator") {
        console.log("Duplicated operator key");
        keys[lastItem] = `"${item.key}"`;
        setAggregateKeys(keys);
        setKeyPressed("");
      } else if (keys.length === 0) {
        if (keyPressed) {
          keys.push(calcResult, `"${item.key}"`);
          setAggregateKeys(keys);
          setKeyPressed("");
        } else if (calcResult) {
          keys.push(calcResult, `"${item.key}"`);
          setAggregateKeys(keys);
        } else {
          return;
        }
      } else {
        keys.push(calcResult, `"${item.key}"`);
        setAggregateKeys(keys);
        setKeyPressed("");
      }
    } else {
      const keyConcat = `${keyPressed}${item.key}`;
      setKeyPressed(keyConcat);
      setCalcResult(keyConcat);
      console.log({ keyConcat });
    }
    setLastKeyPressed(item);

    console.log({ item, keys, lastItem });
  };

  const onKeyDeleted = () => {
    console.log("Delete key");
    const keys = [...aggregateKeys];
    const key = keys[keys.length - 1];
    console.log({ keys, lastKeyPressed, keyPressed });

    if (keyPressed) {
      if (keyPressed.includes('"')) {
        console.log("clear key pressed: operator");
        setKeyPressed("");
      } else {
        let keyConcat = `${keyPressed}`;
        keyConcat = keyConcat.substring(0, keyConcat.length - 1);
        console.log({ keyConcat });
        setKeyPressed(keyConcat);
        setCalcResult(keyConcat);
      }
    } else if (!key.includes('"')) {
      console.log("clear key pressed: integer");
      setKeyPressed(key);
    } else if (key.includes('"')) {
      if (keys.length === 2) {
        setCalcResult("");
        setKeyPressed("");
      }
      keys.splice(-2, 2);
    }
    setAggregateKeys(keys);
    setLastKeyPressed();
  };

  const onKeyCleared = () => {
    console.log("Clear screen");
    setAggregateKeys([]);
    setCalcResult("");
    setKeyPressed("");
    setLastKeyPressed();
  };

  const onCalculate = () => {
    console.log({ aggregateKeys });
    let parseKeys = "";
    aggregateKeys.forEach(item => {
      parseKeys += item + " ";
    });
    parseKeys += keyPressed;

    console.log({ parseKeys });
    if (isElectron()) {
      window.ipcRenderer.send("invokeAction", parseKeys);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Grid
        direction="column"
        xs={12}
        style={{
          backgroundColor: theme.palette.background.default,
          flex: 1,
          flexGrow: 1,
          width: windowWidth,
          height: windowHeight
        }}
      >
        <ResultsScreen result={calcResult} />
        <Keypad
          onKeyPress={onKeyPress}
          onKeyDeleted={onKeyDeleted}
          onKeyCleared={onKeyCleared}
          onCalculate={onCalculate}
          lastKeyPressed={lastKeyPressed}
        />
      </Grid>
    </ThemeProvider>
  );
}

export default App;
