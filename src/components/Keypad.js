import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { makeStyles, useTheme } from "@material-ui/core/styles";

import { resultScreenHeight, windowHeight } from "../constants/window";

const useStyles = makeStyles(theme => ({
  root: {
    background: theme.palette.background.default,
    border: 0,
    color: theme.palette.common.white
  },
  operation: {
    color: `${theme.palette.common.white}50`
  },
  button: {
    borderRadius: 0
  }
}));

export default function Keypad({
  result,
  operation,
  onKeyPress,
  onKeyDeleted,
  onKeyCleared,
  onCalculate,
  lastKeyPressed
}) {
  const classes = useStyles();
  const theme = useTheme();

  const keypadButtons = [
    [
      {
        key: "C",
        type: "clear",
        onClick: onKeyCleared
      },
      {
        key: "9",
        type: "integer"
      },
      {
        key: "6",
        type: "integer"
      },
      {
        key: "3",
        type: "integer"
      },
      {
        key: "%",
        type: "operator"
      }
    ],
    [
      {
        key: "/",
        type: "operator"
      },
      {
        key: "8",
        type: "integer"
      },
      {
        key: "5",
        type: "integer"
      },
      {
        key: "2",
        type: "integer"
      },
      {
        key: "0",
        type: "integer"
      }
    ],
    [
      {
        key: "*",
        type: "operator"
      },
      {
        key: "7",
        type: "integer"
      },
      {
        key: "4",
        type: "integer"
      },
      {
        key: "1",
        type: "integer"
      },
      {
        key: ".",
        type: "period"
      }
    ],
    [
      {
        key: "D",
        type: "delete",
        onClick: onKeyDeleted
      },
      {
        key: "-",
        type: "operator"
      },
      {
        key: "+",
        type: "operator"
      },
      {
        key: "=",
        type: "submit",
        onClick: onCalculate
      }
    ]
  ];

  return (
    <Grid
      container
      direction="row"
      alignItems="flex-start"
      justify="center"
      className={classes.root}
    >
      {keypadButtons.map(row => (
        <Grid container item direction="column" xs={3}>
          {row.map(item => {
            const keyHeight = (windowHeight - resultScreenHeight) / 5;
            return (
              <Grid item xs={12}>
                <Button
                  className={classes.button}
                  variant="contained"
                  color={item.type === "submit" ? "primary" : "secondary"}
                  fullWidth
                  style={{
                    // padding: 0,
                    background:
                      lastKeyPressed &&
                      lastKeyPressed.type === "operator" &&
                      lastKeyPressed &&
                      lastKeyPressed.key === item.key
                        ? `${theme.palette.background.paper}`
                        : undefined,
                    fontSize: theme.typography.subtitle1.fontSize,
                    height: item.type === "submit" ? keyHeight * 2 : keyHeight
                  }}
                  onClick={item.onClick || onKeyPress(item)}
                >
                  {item.key}
                </Button>
              </Grid>
            );
          })}
        </Grid>
      ))}
    </Grid>
  );
}
