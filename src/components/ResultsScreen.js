import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";

import { resultScreenHeight } from "../constants/window";

const useStyles = makeStyles(theme => ({
  root: {
    background: "linear-gradient(45deg, #FE6B8B 30%, #FF8E53 90%)",
    border: 0,
    color: theme.palette.common.white,
    height: resultScreenHeight,
    padding: theme.spacing(0, 1),
    webkitAppRegion: "drag"
  }
}));

export default function ResultsScreen({ result }) {
  const classes = useStyles();
  return (
    <Grid
      container
      direction="column"
      alignItems="flex-end"
      justify="center"
      className={classes.root}
    >
      <Grid item alignItems="flex-end">
        <Typography variant="h3">{result}</Typography>
      </Grid>
    </Grid>
  );
}
