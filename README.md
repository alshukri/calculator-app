![App Preview](preview.png)

# Simple Calculator App
Calculator app consists of two services:
- service written in Golang to do the calculation which accepts 3 argument in the following format: `float "operator" float`
- User interface written in Javascript which execute calculation service


## Technologies Used:

- [React.js](https://reactjs.org/)
- [Electron](https://www.electronjs.org/)
- [Golang](https://golang.org/)


## File Structure
- `service`: contains Golang service responsible for calculation the result.
- `src`: contains ReactJS components
- `public`: contains main Electron process and files required to build react app


## Running App (Dev mode)

1. Install all dependencies: `npm install`
1. Start dev server: `npm start`
1. Start Electron: `npm run electron-start`

## Building App
Bundled app is not provided due to files size. Run `npm run electron-pack`, to build the app. Then head over to `dist` directory

**NOTE:** Although Windows, Linux and Mac OS apps are provided but it was tested on Mac OS only.