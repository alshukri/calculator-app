package main

import (
	"fmt"
	"os"
	"strconv"
)

func operation(num1, num2 float64) float64 {
	operator := os.Args[2]
	switch operator {
	case "+":
		return num1 + num2
	case "-":
		return num1 - num2
	case "*":
		return num1 * num2
	case "/":
		return num1 / num2
	default:
		return 0
	}
}

func main() {
	var sum float64

	n, _ := strconv.ParseFloat(os.Args[1], 10)
	m, _ := strconv.ParseFloat(os.Args[3], 10)

	sum = operation(n, m)
	fmt.Print(sum)
}
